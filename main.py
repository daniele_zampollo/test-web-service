from flask import Flask, request, make_response, jsonify
from flask_restful import Api, reqparse
from werkzeug.utils import secure_filename

import logging
import datetime
import sys
import re
import os
import botocore
import boto3

app = Flask(__name__)
api = Api(app)

## LOGGER
log = logging.getLogger('__main__')
log.setLevel(logging.INFO)

formatter = logging.Formatter(
    '%(asctime)s,%(msecs)03d [%(module)s:%(process)d] [%(levelname)-7s] (%(funcName)s:%(lineno)d) - %(message)s', datefmt = '%Y-%m-%d %H:%M:%S')

console_handler = logging.StreamHandler(sys.stdout)
console_handler.setFormatter(formatter)
log.addHandler(console_handler)

## upload folder
UPLOAD_FOLDER = '/uploads'
os.mkdir(UPLOAD_FOLDER)

## Target S3 bucket as environment variable
S3_BUCKET = os.environ.get('S3_BUCKET')


@app.route('/api/health')
def hello_world():
    return make_response('{"status": "ok"}')

@app.route('/api/upload-random', methods=['POST'])
def upload_file():
    # check if the post request has the file part
    if 'file' not in request.files:
        return make_response('{"successful": "false", "message": "file missing"}'), 400
    file = request.files['file']
    if file:
        filename = secure_filename(file.filename)
        file.save(os.path.join(UPLOAD_FOLDER, filename))

        try:
            upload_s3(UPLOAD_FOLDER + '/' + filename)
            return  make_response('{"successful": "true", "message": "file uploaded"}')
        except botocore.exceptions.ClientError as error:
            return  make_response('{"successful": "false", "message": "' + error.response['Error']['Message'] +'"}'), 400

    return make_response('{"successful": "false", "message": "wrong request"}'), 400

def upload_s3(file):
    s3 = boto3.resource('s3')
    data = open(file, 'rb')
    s3.Bucket(S3_BUCKET).put_object(Key=os.path.basename(file), Body=data)
    return True

if __name__ == "__main__":
  app.run(host='0.0.0.0', port=4545)
