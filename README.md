## Notes
- The Terraform scripts creates a simple S3 bucket; I didin't create any specific IAM identity or ACL. The only variable is the bucket name
- The web server has been implemented in Python, using Flask. Not to be used for production purposes, but perfect for testing or development
- I didn't spend much time in implementing the upload API, but probably it could be improved, for example, by adding more checks on the uploaded files
- About CI/CD tool, I choosed Jenkins: it's the one I'm more familiar with, in this moment
- To deploy the application, I choosed Helm, because I'm more familiar with it, but in this specific case it would make more sense to use Terraform, to manage both the S3 bucket creation and the application deploy in the same Terrafor module
- The Helm package does not include the dockerconfigjson secret, needed for pulling the docker image from the private registry; it must be manually created in the target namespace before deploying the Helm package

## Bonus
If this simple application should be deployed for several customers, I would probably implement a single Terraform module to manage both the cloud storage creation and the Kubernetes deploy. The module's variable would probably be:

- AWS:
    - bucket name
    - IAM role
- Kubernetes:
    - namespace
    - ingress URL
    - docker image
    - number of replicas

I would also integrate everything with a secret management tool like Vault to manage secrets