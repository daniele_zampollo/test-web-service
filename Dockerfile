FROM python:3.9

COPY requirements.txt /app/

WORKDIR /app

RUN pip install -r requirements.txt
COPY main.py uwsgi.ini /app/

ENTRYPOINT [ "python" ]

# run when running the container
CMD [ "main.py" ]
