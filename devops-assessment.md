## Problem statement

The assessment includes challenges across the whole stack that we are using at Oper for DevOps-related tasks. We would like to test skills in:

- CI/CD
- Kubernetes
- Infrastructure as Code
- Web server coding

We would like to assess how the candidate can space among different tools and technologies, with how much clarity and order while requiring them to think about delivering an end-to-end solution.

The candidate should use the technologies that are familiar to Oper (highlighted in **bold** in the list below), but some alternatives can be used if he/she has already lot of experience with them and none instead with our preferred ones.

- CI/CD → **Gitlab CI** - (Alternatives: Github Actions or Jenkins)
- Coding → **Python / NodeJS** - (Alternatives: PHP or Java)
- Cloud Provider → **AWS / GCP** - (Alternatives: some **free** storage solution deployable with Terraform)
- Infrastructure as Code → **Terraform**
- Kubernetes deployments → **Helm / Kubectl / Terraform**



**Tasks**:

1. Using Terraform, define the infrastructure that enables an application to programmatically upload files to a cloud storage.
2. Develop a web server that exposes on the port `4545` the following endpoints:
    1. GET `/api/health` returns a JSON payload containing `{ "status": "ok" }`
    2. POST `/api/upload-random` uploads a randomly-named file (content of the file is not relevant) to the storage created before.
3. Using CI, build a Docker image of the application and push it to a registry any time there is a push on the repo.
4. Deploy the application on a K8s cluster (you can use [MiniKube](https://minikube.sigs.k8s.io/docs/) for spinning a cluster locally) using helm / kubectl / terraform. The deliverable should:
    - Use the docker image you pushed to the registry
    - Provide an Ingress that listens on port 80 and redirect traffic to the application

**Note:** Creation of real infrastructure can be skipped since it might incur in costs / creation of the accounts / credit card which is out of scope for this assignment. 

**Bonus:** Imagine having to serve 3 customers. Each customer wants to have their own instance, with their own storage, etc...
Try to define with your own words (no code) the improvements and generalization that your setup must go through in order to deliver a stable and scalable solution, fit for serving a generic number of customers.